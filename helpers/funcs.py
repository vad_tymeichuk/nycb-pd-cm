# from selenium import webdriver

from start_point import browser
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException, NoSuchElementException
from helpers import utils
from helpers.utils import screenshot_name

# Possible exceptions to use in except statement: TimeoutException, ElementClickInterceptedException, NoSuchElementException
# This makes browser wait n seconds before presence of an element gets located
# The explicit function time.sleep(n) can also be used in every function as a substitute - "import time" in this case needed
wait = WebDriverWait(browser, 15)

def feb_cn (class_n):
    try:
        link = wait.until(EC.presence_of_element_located((By.CLASS_NAME, class_n)))
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name
    return link.click()

def feb_lt (link_t):
    try:
        link = wait.until(EC.presence_of_element_located((By.LINK_TEXT, link_t)))
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name
    return link.click()

def feb_xp (xp):
    try:
        link = wait.until(EC.presence_of_element_located((By.XPATH, xp)))
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name
    return link.click()

def feb_id (id):
    try:
        link = wait.until(EC.presence_of_element_located((By.ID, id)))
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name
    return link.click()

def feb_tn (tn, tn_text):
    try:
        links = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, tn)))
        for el in links:
            if el.text == tn_text:
                return el.click()
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name

def verification(el_type):
    try:
        quantity = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, el_type)))
        for el1 in quantity:
            if el1.get_attribute('class') == 'input-text qty text':
                if el1.get_attribute('value') == "1":
                    text = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, 'small')))
                    for el2 in text:
                        if 'payable in total' in el2.text:
                            assert 'payable in total' in el2.text
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name

def verification_custom(el_type):
    try:
        custom_made = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, el_type)))
        for el1 in custom_made:
            if el1.get_attribute('class') != 'variation-CustomMade':
                continue
            else:
                assert 'CUSTOM MADE +:' in el1.text
            # if el1.get_attribute('class') == 'input-text qty text':
            #     if el1.get_attribute('value') == "1":
            #         text = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, 'small')))
            #         for el2 in text:
    except:
        utils.save_screenshot(browser)
        browser.quit()
        assert False, 'check out screenshot ' + screenshot_name
