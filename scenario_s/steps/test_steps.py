from pytest_bdd import given, when, then
from start_point import browser
from helpers import funcs
import time
import pytest


@given('I am on the main page with having menu opened')
def open_page1():
    funcs.feb_cn('mega-menu-link')

@when('I click on the "all wedding dresses" link')
def open_page2():
    funcs.feb_lt('All wedding dresses')

@when('I click on first product in the list')
def open_page3():
    funcs.feb_cn('woocommerce-loop-product__title')

# @when('I click on first product in the list')
# def open_page3():
#     funcs.feb_lt('WEDDING DRESS .*')

@when('I click on the "Pay deposit" button')
def open_page4():
    funcs.feb_tn('label', 'Pay Deposit')

@when('I click on the "Add to cart" button')
def open_page5():
    funcs.feb_tn('button', 'ADD TO CART')

@when('I click on the "View cart" button')
def open_page6():
    funcs.feb_lt('VIEW CART')

@then('"payable in total" text is against product name in the cart')
def verify():
    try:
        funcs.verification('input')
    finally:
        browser.close()


# #@pytest.fixture()
# def teardown():
#     # yield browser
#     browser.close()