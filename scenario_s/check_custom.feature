Feature: Verify "custom_made" option
  Verify that "custom_made" option has been applied

  Scenario: Apply "custom_made" option
    Given I am on the main page with having menu opened
    When I click on the "all wedding dresses" link
    When I click on first product in the list
    When I click on the "Custom made" checkbox
    When I click on the "Add to cart" button
    When I click on the "View cart" button
    Then "Custom Made +" text is against product name in the cart
